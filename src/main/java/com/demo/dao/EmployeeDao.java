package com.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.util.string.StringValue;
import org.hibernate.Session;

import com.demo.model.EmployeeBean;
import com.demo.model.EmployeeModel;

import util.HibernateUtil;

public class EmployeeDao {

	public Long saveEmployee(EmployeeModel model) {
		try {
			Session session = HibernateUtil.createSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        Long id = (Long) session.save(model);
	        session.getTransaction().commit();
	        return id;
		} catch (Exception e) {
			System.out.println(e);
			return 0L;
		}
		
	}

	public List<EmployeeBean> getAllEmployee() {
		List<EmployeeBean> data = new ArrayList<EmployeeBean>();
		try {
			Session session = HibernateUtil.createSessionFactory().getCurrentSession();
			session.beginTransaction();
			List<EmployeeModel> modelData = session.createQuery("from EmployeeModel").list();
			modelData.forEach(obj -> {
				EmployeeBean bean = new EmployeeBean();;
				bean.setDepartment(obj.getDepartment());
				bean.setEmployeeId(obj.getEmployeeId());
				bean.setEmployeeName(obj.getEmployeeName());
				bean.setSalay(obj.getSalay());
				data.add(bean);
			});
		} catch (Exception e) {
			System.out.println(e);
		}
		return data;
	}

	public boolean deleteEmployee(Long employeeId) {
		try {
			Session session = HibernateUtil.createSessionFactory().getCurrentSession();
	        session.beginTransaction();
			Object persistentInstance = session.load(EmployeeModel.class, employeeId);
			if (persistentInstance != null) {
			    session.delete(persistentInstance);
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
		
		return true;
	}

	public EmployeeBean getEmployeeById(StringValue employeeID) {
		EmployeeBean bean = new EmployeeBean();;
		try {
			Session session = HibernateUtil.createSessionFactory().getCurrentSession();
			session.beginTransaction();
			List<EmployeeModel> modelData = session.createQuery("from EmployeeModel where employeeId=:id")
					.setParameter("id", Long.parseLong(employeeID.toString())).list();
			modelData.forEach(obj -> {
				bean.setDepartment(obj.getDepartment());
				bean.setEmployeeId(obj.getEmployeeId());
				bean.setEmployeeName(obj.getEmployeeName());
				bean.setSalay(obj.getSalay());
			});
		} catch (Exception e) {
			System.out.println(e);
		}
		return bean;
	}

	public EmployeeModel updateEmployee(EmployeeModel model) {
		try {
			Session session = HibernateUtil.createSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        session.update(model);
	        session.getTransaction().commit();
	        return model;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	
	
	
}
