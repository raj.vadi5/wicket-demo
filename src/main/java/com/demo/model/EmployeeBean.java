package com.demo.model;

import java.io.Serializable;

public class EmployeeBean implements Serializable {

	private static final long serialVersionUID = -6499068573555233416L;

	private Long employeeId;
	private String employeeName;
	private String department;
	private Double salay;
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public Double getSalay() {
		return salay;
	}
	public void setSalay(Double salay) {
		this.salay = salay;
	}
	@Override
	public String toString() {
		return "EmployeeBean [employeeId=" + employeeId + ", employeeName=" + employeeName + ", department="
				+ department + ", salay=" + salay + "]";
	}
	
	
	
}
