package com.demo;

import java.util.Arrays;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormSubmitBehavior;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.demo.dao.EmployeeDao;
import com.demo.model.EmployeeBean;
import com.demo.model.EmployeeModel;


public class EmployeeEdit extends WebPage {

	private static final List<String> DEPARTMENTS = Arrays.asList(new String[] {
            "Development", "Testing", "Accounts" });

	private String selected = DEPARTMENTS.get(0);
	public EmployeeEdit(PageParameters parameters) {
		super(parameters);
		EmployeeBean employeeBean = new EmployeeDao().getEmployeeById(parameters.get("emp_id"));
		add(new EmployeeForm("employeeForm",employeeBean));
		//System.out.println("testtst"+parameters.get("emp_id"));
		add(new Link<Void>("employeePage") {
			@Override
			public void onClick() {
				System.out.println("Redirecting to employee page");
				setResponsePage(EmployeePage.class);
			}
		});
	}
	
	 private class EmployeeForm extends Form<Void> {

		private Long id;
		private String employeeName;
		private Double salary;
		DropDownChoice<String> department = new DropDownChoice<String>("department",
				 new Model<String>(), DEPARTMENTS);

		public EmployeeForm(String id, EmployeeBean employeeBean) {
			super(id);
			this.id = employeeBean.getEmployeeId();
			add(new RequiredTextField<>("employeeName", new Model<String>(employeeBean.getEmployeeName()))
					.setOutputMarkupId(true));
			department = new DropDownChoice<String>("department", new Model<String>(employeeBean.getDepartment()),
					DEPARTMENTS);
			add(department.setOutputMarkupId(true));
			add(new NumberTextField<>("salary", new Model<Double>(employeeBean.getSalay())).setOutputMarkupId(true));
			add(new FeedbackPanel("feedback"));
			setModel(new CompoundPropertyModel(this));
			this.add(new AjaxFormSubmitBehavior("onsubmit") {
	            @Override
	            protected void onSubmit(AjaxRequestTarget target) {
	                super.onSubmit(target); 
	                System.out.println(getRequest().getRequestParameters().getParameterValue("emp_id"));
	                EmployeeModel model = new EmployeeModel();
	                model.setEmployeeId(getRequest().getRequestParameters().getParameterValue("emp_id").toLong());
	                model.setEmployeeName(getRequest().getRequestParameters().getParameterValue("employeeName").toString());
	                model.setDepartment(department.getModelObject());
	                model.setSalay(getRequest().getRequestParameters().getParameterValue("salary").toDouble());
	                EmployeeModel result = new EmployeeDao().updateEmployee(model);
	                if(result!=null)
	                	info("Updated Successfully");
	                else
	   	    		 	error("Unable to save");
	            }
	        });
		}
		
		
		
		/*@Override
	     protected void onSubmit() {
	    	 EmployeeModel model = new EmployeeModel();
	    	 model.setEmployeeId(this.id);
	    	 model.setEmployeeName(employeeName);
	    	 model.setDepartment(department.getModelObject());
	    	 model.setSalay(salary);
	    	 EmployeeModel result = new EmployeeDao().updateEmployee(model);
	    	 if(result!=null)
	    		 info("Updated Successfully");
	    	 else
	    		 error("Unable to save");
	     }*/
		 
	 }
	
	
}
