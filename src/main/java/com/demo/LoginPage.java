package com.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.flow.RedirectToUrlException;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

public class LoginPage extends WebPage {

    private static Logger logger = LogManager.getLogger(LoginPage.class);

    public LoginPage(PageParameters parameters) {
        super(parameters);
        add(new LoginForm("loginForm"));
    }

    private class LoginForm extends Form<Void> {
        private transient RequestCache requestCache = new HttpSessionRequestCache();

        private String username;

        private String password;

        public LoginForm(String id) {
            super(id);
            setModel(new CompoundPropertyModel(this));
            add(new RequiredTextField<>("username"));
            add(new PasswordTextField("password"));
            add(new FeedbackPanel("feedback"));
        }

        @Override
        protected void onSubmit() {
            HttpServletRequest servletRequest = (HttpServletRequest) RequestCycle.get().getRequest().getContainerRequest();
            String originalUrl = getOriginalUrl(servletRequest.getSession());
            AuthenticatedWebSession session = AuthenticatedWebSession.get();
            try {
                boolean signin = session.signIn(username, password);
                if (signin) {
                    if (originalUrl != null) {
                        logger.info(String.format("redirecting to %s", originalUrl));
                        throw new RedirectToUrlException(originalUrl);
                    } else {
                        logger.info("redirecting to home page");
                        //setResponsePage(getApplication().getHomePage());
                        setResponsePage(EmployeePage.class);
                    }
                }
            } catch (AuthenticationException e) {
                error(e.getMessage());
            }
        }


        private String getOriginalUrl(HttpSession session) {
            SavedRequest savedRequest = (SavedRequest) session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
            if (savedRequest != null) {
                return savedRequest.getRedirectUrl();
            } else {
                return null;
            }
        }
    }

}
