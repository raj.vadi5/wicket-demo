package com.demo;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.demo.dao.EmployeeDao;
import com.demo.model.EmployeeBean;

public class EmployeePage extends WebPage {

	private static Logger logger = LogManager.getLogger(LoginPage.class);
	
    public EmployeePage(PageParameters parameters) {
        super(parameters);

        List<EmployeeBean> employeeBean = new EmployeeDao().getAllEmployee();
        //System.out.println(employeeBean);
        add(new ListView<EmployeeBean>("listView", employeeBean)
        {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public void populateItem(final ListItem<EmployeeBean> item)
               {
                       final EmployeeBean data= item.getModelObject();
                       item.add(new Label("employeeId", data.getEmployeeId()));
                       item.add(new Label("employeeName", data.getEmployeeName()));
                       item.add(new Label("department", data.getDepartment()));
                       item.add(new Label("salary", data.getSalay()));
                       item.add(new Link("editLink", item.getModel()) {

						private static final long serialVersionUID = 2359568280612579624L;

						@Override
                           public void onClick() {
                        	   EmployeeBean employee= (EmployeeBean) getModelObject();
                               System.out.println(employee.getEmployeeId());
                               parameters.add("emp_id", employee.getEmployeeId());
                               setResponsePage(EmployeeEdit.class, parameters);
                           }
                       });
                       
                       item.add(new Link("deleteLink", item.getModel()) {
						private static final long serialVersionUID = -8093672806688824276L;

							  @Override
                              public void onClick() {
                           	   EmployeeBean employee= (EmployeeBean) getModelObject();
                                  System.out.println(employee.getEmployeeId());
                           	   	  boolean flag = new EmployeeDao().deleteEmployee(employee.getEmployeeId());
                           	   	  //boolean flag=false;
                           	   	  if(flag)
                           	   		  setResponsePage(EmployeePage.class);
                           	   	  else
                           	   		 error("Unable to delete");
                           	   		 
                           	   	 
                              }
                       });
               }
        });
        
        /*Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof User) {
            String username = ((User) principal).getUsername();
            add(new Label("username", username));
        } else {
            add(new Label("username", "error"));
        }*/

       /* add(new Link<Void>("logoutLink"){

            @Override
            public void onClick() {
                getSession().invalidate();
            }
        });*/
        
        
        final Form<Void> form = new Form<Void>("form");
		this.add(form);
		
		form.add(new Button("addEmployeeBtn", Model.of("Add Employee")) { 

			private static final long serialVersionUID = 1L;

			/*@Override
			protected String getOnClickScript()
			{
				return "alert('The button has been clicked!');";
			}*/

			@Override
			public void onSubmit()
			{
				logger.info("Add Employee...");
				setResponsePage(AddEmployee.class);
			}
		});
		
		form.add(new Button("logoutLink", Model.of("Logout")) { 
			private static final long serialVersionUID = 1L;
			@Override
			public void onSubmit()
			{
				logger.info("logging out....");
				getSession().invalidate();
				logger.info("logged out successfully....");
			}
		});
		
        
    }

}
