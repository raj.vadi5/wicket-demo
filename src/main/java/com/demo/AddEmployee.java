package com.demo;

import java.awt.TextField;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.demo.dao.EmployeeDao;
import com.demo.model.EmployeeModel;


public class AddEmployee extends WebPage {
	
	private static Logger logger = LogManager.getLogger(AddEmployee.class);
	
	private static final List<String> DEPARTMENTS = Arrays.asList(new String[] {
            "Development", "Testing", "Accounts" });

	private String selected = DEPARTMENTS.get(0);
	
	public AddEmployee(PageParameters parameters) {
        super(parameters);
        add(new EmployeeForm("employeeForm"));
        add(new Link<Void>("employeePage") {
			@Override
			public void onClick() {
				System.out.println("Redirecting to employee page");
				setResponsePage(EmployeePage.class);
			}
		});
    }
	
	 private class EmployeeForm extends Form<Void> {
		 private String employeeName;
	    // private String department;
	     private Double salary;
			
			 DropDownChoice<String> department = new DropDownChoice<String>("department",
					 new Model<String>(), DEPARTMENTS);
			 
	    
	     public EmployeeForm(String id) {
	            super(id);
	            setModel(new CompoundPropertyModel(this));
	            add(new RequiredTextField<>("employeeName"));
	            add(department);
	            add(new NumberTextField<>("salary"));
	            add(new FeedbackPanel("feedback"));
	            
	     }
	     
	     @Override
	     protected void onSubmit() {
	    	 EmployeeModel model = new EmployeeModel();
	    	 model.setEmployeeName(employeeName);
	    	 model.setDepartment(department.getModelObject());
	    	 model.setSalay(salary);
	    	 Long id = new EmployeeDao().saveEmployee(model);
	    	 // Long id=1L;
	    	 if(id>0)
	    		 info("Successfully saved");
	    	 else
	    		 error("Unable to save");
	    	 //Need to clear form
	    	 logger.info("Adding Employee.....");
	     }
	 }
	
	
}
