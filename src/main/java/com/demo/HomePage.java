package com.demo;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.WebPage;

public class HomePage extends WebPage {

	public HomePage(final PageParameters parameters) {
		super(parameters);
		add(new Link<Void>("login") {
			@Override
			public void onClick() {
				System.out.println("Redirecting to login page");
				setResponsePage(LoginPage.class);
			}
		});
    }
}
